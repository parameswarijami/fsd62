package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/GetAllEmployees")
public class GetAllEmployees extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		EmployeeDao empDao = new EmployeeDao();
		List<Employee> empList = empDao.getAllEmployees();
	
		if (empList != null) {			
			request.setAttribute("empList", empList);			
			request.getRequestDispatcher("GetAllEmployees.jsp").forward(request, response);	
		} else {
			request.getRequestDispatcher("HRHomePage.jsp").include(request, response);
			out.println("<h3 style='color:red; align='center'>Unable to Fetch Employee Records!!</h3>");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}

















//package com.web;
//
//import java.util.List;
//import java.io.IOException;
//import java.io.PrintWriter;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import com.dao.EmployeeDao;
//import com.dto.Employee;
//
//@WebServlet("/GetAllEmp")
//public class GetAllEmp extends HttpServlet {
//	
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		response.setContentType("text/html");
//		PrintWriter out = response.getWriter();
//		
//		
//		EmployeeDao empDao = new EmployeeDao();
//		List<Employee> empList = empDao.getAllEmployee();
//
//		out.println("<html>");
//		out.println("<body bgcolor='lightyellow' text='green'>");
//		
//		
//		RequestDispatcher rd = request.getRequestDispatcher("HRHomePage");
//		rd.include(request, response);
//		out.println("<br/>");
//		out.println("<center>");
//		if (!empList.isEmpty()) {
//			out.println("<table align='center' border=2>");
//			
//			out.println("<tr>");
//			out.println("<th>EmpId</th>");
//			out.println("<th>EmpName</th>");
//			out.println("<th>Salary</th>");
//			out.println("<th>Gender</th>");
//			out.println("<th>EmailId</th>");
//			out.println("</tr>");
//			
//			for (Employee emp : empList) {
//			out.println("<tr>");
//			out.println("<td>" + emp.getEmpId()   + "</td>");
//			out.println("<td>" + emp.getEmpName() + "</td>");
//			out.println("<td>" + emp.getSalary()  + "</td>");
//			out.println("<td>" + emp.getGender()  + "</td>");
//			out.println("<td>" + emp.getEmailId() + "</td>");
//
//			out.println("</tr>");
//			}
//			out.println("</table>");
//		} else {
//			out.println("<h3 style='color:red;'>Employee Record Not Found!!!</h3>");
//		}
//		out.println("</center></body></html>");
//	}
//	
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		doGet(request, response);
//	}
//}