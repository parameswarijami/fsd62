package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/HRHomePage")
public class HRHomePage extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String emailId = request.getParameter("emailId");
		
		out.println("<html>");
		out.println("<body bgcolor='lightyellow' text='green'>");
		
		//Providing Home & Logout Links
		out.println("<form align='right'>");
		out.println("<a href='HRHomePage'>Home</a> &nbsp;");
		out.println("<a href='Login.html'>Logout</a>");
		out.println("</form>");
		
		out.println("<h3>Welcome: " + emailId + "! </h3>");
		
		out.println("<center>");		
		out.println("<h1>Welcome to HRHomePage</h1>");	
		
		//Adding the Links to HRHomePage
		out.println("<h3>");
		out.println("<a href='GetEmpById.html'>GetEmployeeById</a> &nbsp; &nbsp; ");
		out.println("<a href='GetAllEmp'>GetAllEmployees</a>");
		out.println("</h3>");		
		
		out.println("</center></body></html>");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}