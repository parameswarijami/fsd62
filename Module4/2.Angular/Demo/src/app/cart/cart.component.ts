import { JsonPipe } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent {

  cartProducts: any;
  products: any;
  emailId: any;
  total: number;
  
  constructor(private router: Router) {

    this.total = 0;

    this.emailId = localStorage.getItem('emailId');
    this.products  = localStorage.getItem('cartProducts');
    this.cartProducts = JSON.parse(this.products);

    console.log(this.cartProducts);


    if (this.cartProducts != null) {
      this.cartProducts.forEach((element: any) => {
        this.total += element.price;
      });
    }
  }

  purchase() {

    alert('Thank You for Purchasing');

    this.cartProducts = [];
    localStorage.removeItem('cartProducts');
    this.router.navigate(['products']);
  }

}

