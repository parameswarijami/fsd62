import { Component } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  employees: any;
  emp: any;

  constructor(private service: EmpService, private router: Router) {

    this.employees = [
      {empId:101, empName:'Harsha', salary:1212.12, gender:'Male',   country:'IND', doj:'06-13-2018', emailId: 'harsha@gmail.com', password: '123'},
      {empId:102, empName:'Pasha',  salary:2323.23, gender:'Male',   country:'CHI', doj:'07-14-2017', emailId: 'pasha@gmail.com', password: '123'},
      {empId:103, empName:'Indira', salary:3434.34, gender:'Female', country:'USA', doj:'08-15-2016', emailId: 'indira@gmail.com', password: '123'},
      {empId:104, empName:'Navya',  salary:4545.45, gender:'Female', country:'FRA', doj:'09-16-2015', emailId: 'navya@gmail.com', password: '123'},
      {empId:105, empName:'Vamsi',  salary:5656.56, gender:'Male',   country:'NEP', doj:'10-17-2014', emailId: 'vamsi@gmail.com', password: '123'}
    ];

  }

  loginSubmit(loginForm: any) {
    console.log(loginForm);

    //Setting EmailId under LocalStorage
    localStorage.setItem('emailId', loginForm.emailId);

    if (loginForm.emailId == "HR" && loginForm.password == "HR") {
      this.service.setUserLoggedIn();
      this.router.navigate(['showemps']);
    } else {

      this.emp = null;

      this.employees.forEach((element: any) => {
        if (element.emailId == loginForm.emailId && element.password == loginForm.password) {
          this.emp = element;
        }
      });

      if (this.emp != null) {
        this.service.setUserLoggedIn();
        this.router.navigate(['products']);
      } else {
        alert("Invalid Credentials");
      }
    }
  }
}

