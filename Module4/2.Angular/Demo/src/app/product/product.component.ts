import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit {

  products: any;
  emailId: any;
  cartProducts: any;

  constructor(private service: EmpService) {

    //Making the CartProducts as an Array of Products
    this.cartProducts = [];
    this.emailId = localStorage.getItem('emailId'); 
  }
  ngOnInit(): void {
    this.service.getAllProducts().subscribe((data:any)=>{
      console.log(data);
      this.products=data;
    })
  }

  addToCart(product: any) {
    this.cartProducts.push(product);

    localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
  }

}




// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-product',
//   templateUrl: './product.component.html',
//   styleUrls: ['./product.component.css']
// })
// export class ProductComponent {
//   products: any[];
//   emailId: string | null;
//   cartProducts: any[];

//   constructor() {
//     // Initialize the cart products array
//     this.cartProducts = [];

//     // Get the email ID from localStorage
//     this.emailId = localStorage.getItem('emailId');

//     // Define the list of products
//     this.products = [
//       { id: 1001, name: "Nokia", price: 14999.00, description: "No Cost EMI Applicable", imgsrc: "assets/Images/10001.jpg" },
//       { id: 1002, name: "Samsung", price: 24999.00, description: "No Cost EMI Applicable", imgsrc: "assets/Images/10002.jpg" },
//       { id: 1003, name: "IPhone", price: 34999.00, description: "No Cost EMI Applicable", imgsrc: "assets/Images/10003.jpg" },
//       { id: 1004, name: "RealMe", price: 44999.00, description: "No Cost EMI Applicable", imgsrc: "assets/Images/10004.jpg" },
//       { id: 1005, name: "Oppo", price: 54999.00, description: "No Cost EMI Applicable", imgsrc: "assets/Images/10005.jpg" },
//       { id: 1006, name: "Vivo", price: 64999.00, description: "No Cost EMI Applicable", imgsrc: "assets/Images/10006.jpg" }
//     ];
//   }

//   addToCart(product: any) {
//     // Add the product to the cart
//     this.cartProducts.push(product);

//     // Store the updated cart in localStorage
//     localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
//   }
// }
