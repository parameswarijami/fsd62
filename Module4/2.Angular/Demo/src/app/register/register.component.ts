import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {

  employee: any;
  countries: any;
  departments: any;

  constructor(private service: EmpService) {

    //Remove the empId attribute from the employee Object    
    this.employee = {
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',

      department: {
        deptId: ''
      }

    }
  }

  ngOnInit() {
    this.service.getAllContries().subscribe((data: any) => { this.countries = data; });
    
    //Fetching all departments
    this.service.getAllDepartments().subscribe((data: any) => { this.departments = data; });
  }


  submit() {
    console.log(this.employee);
    this.service.registerEmployee(this.employee).subscribe((data: any) => {
      console.log(data);
    });
  }

}
