package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Employee;

@Service
public class EmployeeDao {

	@Autowired
	EmployeeRepository empRepo;
	
	public Employee empLogin(String emailId, String password) {
		return empRepo.empLogin(emailId, password);
	}

	public List<Employee> getAllEmployees() {
		return empRepo.findAll();
	}

	public Employee getEmployeeById(int empId) {
		return empRepo.findById(empId).orElse(null);
	}

	public List<Employee> getEmployeeByName(String empName) {
		return empRepo.findByName(empName);
	}

	public Employee addEmployee(Employee emp) {
		return empRepo.save(emp);
	}
	
	public Employee updateEmployee(Employee emp) {
		return empRepo.save(emp);
	}

	public void deleteEmployeeById(int empId) {
		empRepo.deleteById(empId);
	}
	
}
