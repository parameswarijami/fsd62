package com.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {
	
	@Autowired
	StudentDao studDao;
	
	@GetMapping("studLogin/{emailId}/{password}")
	public Student studLogin(@PathVariable("emailId") String emailId, 
			@PathVariable("password") String password) {
		
		return studDao.studLogin(emailId, password);
	}
	
	@GetMapping("getAllStudents")
	public List<Student> getAllStudents() {
		return studDao.getAllStudents();
	}
	
	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int studId) {
		return studDao.getStudentById(studId);
	}
	
	@GetMapping("getStudentByName/{name}")
	public List<Student> getStudentByName(@PathVariable("name") String studName) {
		return studDao.getStudentByName(studName);
	}
	
//	@PostMapping("addStudent")
//	public ResponseEntity<?> addStudent(@RequestBody Student stud) {
//	    try {
//	        Student newStudent = studDao.addStudent(stud); 
//	        return ResponseEntity.ok(newStudent); 
//	    } catch (DataIntegrityViolationException e) { 
//	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email ID already exists"); 
//	    }
//	}
	@PostMapping("addStudent")
	public ResponseEntity<?> addStudent(@RequestBody Student stud) {
	    try {
	        // Normalize email and check uniqueness
	        stud.setEmailId(stud.getEmailId().trim().toLowerCase());

	        // Generate OTP
	        String otp = generateOTP();
	        stud.setOtp(otp);

	        Student newStudent = studDao.addStudent(stud);

	        // Send confirmation email
	        emailService.sendConfirmationEmail(newStudent.getEmailId());

	        // Send OTP
	        otpService.sendOTP(newStudent.getEmailId(), otp);

	        return ResponseEntity.ok(newStudent);
	    } catch (DataIntegrityViolationException e) {
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email ID already exists");
	    }
	}

	@PutMapping("updateStudent")
	public Student updateStudent(@RequestBody Student stud) {
		return studDao.updateStudent(stud);
	}
	
	@DeleteMapping("deleteStudentById/{id}")
	public String deleteStudentById(@PathVariable("id") int studId) {
		studDao.deleteStudentById(studId);
		return "Employee Record Deleted Successfully!!!";
	}
}

