package com.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;
import com.service.EmailService;
import com.service.OtpService;

import com.dao.CourseDao;
import com.model.Course;

@RestController
public class StudentController {
	
	 @Autowired
	    private StudentDao studDao;

	    @Autowired
	    private EmailService emailService;

	    @Autowired
	    private OtpService otpService;
	    
	    @Autowired
	    private CourseDao courseDao;
	
	@GetMapping("studLogin/{emailId}/{password}")
	public Student studLogin(@PathVariable("emailId") String emailId, 
			@PathVariable("password") String password) {
		
		return studDao.studLogin(emailId, password);
	}
	
	@GetMapping("getAllStudents")
	public List<Student> getAllStudents() {
		return studDao.getAllStudents();
	}
	
	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int studId) {
		return studDao.getStudentById(studId);
	}
	
	@GetMapping("getStudentByName/{name}")
	public List<Student> getStudentByName(@PathVariable("name") String studName) {
		return studDao.getStudentByName(studName);
	}
	
//	@PostMapping("addStudent")
//	public ResponseEntity<?> addStudent(@RequestBody Student stud) {
//	    try {
//	        Student newStudent = studDao.addStudent(stud); 
//	        return ResponseEntity.ok(newStudent); 
//	    } catch (DataIntegrityViolationException e) { 
//	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email ID already exists"); 
//	    }
//	}
	@PostMapping("addStudent")
	public ResponseEntity<?> addStudent(@RequestBody Student stud) {
	    try {
	        // Normalize and check email
	        String email = stud.getEmailId().trim().toLowerCase();
	        if (studDao.getStudentByEmail(email) != null) {
	            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email ID already exists");
	        }

	        // Find and validate course
	        Course course = courseDao.getCourseById(stud.getCourseId());
	        if (course == null) {
	            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid course ID");
	        }
	        stud.setCourse(course);

	        // Save the student
	        Student newStudent = studDao.addStudent(stud);

	        return ResponseEntity.ok(newStudent);
	    } catch (Exception e) {
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred: " + e.getMessage());
	    }
	}

	@PutMapping("updateStudent")
	public Student updateStudent(@RequestBody Student stud) {
		return studDao.updateStudent(stud);
	}
	
	@DeleteMapping("deleteStudentById/{id}")
	public String deleteStudentById(@PathVariable("id") int studId) {
		studDao.deleteStudentById(studId);
		return "Employee Record Deleted Successfully!!!";
	}
}

